<section id="backend">
    <h2>Backend <small class="subhead">Der Lebensraum des Administrators</small>
    </h2>
    <p>
        Das Backend ist die Administrationsoberfläche der Homepage. Hier verwalten Sie alle Inhalte, Menüs und Einstellungen – einfach Alles …
    </p>
    <p>
        Wie schon erwähnt werden Sie in Zukunft wahrscheinlich einen Großteil der Zeit nicht im Frontend sondern im Backend verbringen. Sie sollten sich hier also gut auskennen.
    </p>
    <h3 id="wp-admin">WP-Admin <small class="subhead">Der Weg ins Backend</small>
    </h3>
    <p>
        Die erste Frage ist <em>»Wie gelangt man überhaupt ins Backend?«</em>. Diese Frage ist im Grunde schnell beantwortet. Befinden Sie sich auf der Startseite der Homepage
        hängen Sie an die URL <code>/wp-admin</code> an.
    </p>
    <p>
        Für die Homepage mit der Domain <samp>www.testdomain.de</samp> ergibt sich also <samp>www.testdomain.de/wp-admin</samp>.
    </p>
    <p>
        Befindet sich die Homepage in einem Unterverzeichnis der Hauptdomain funktioniert das genauso: <samp>www.fritzedesign.de/verzeichnis/wp-admin</samp>.
    </p>
    <p class="small">
        Die Entwickler von WordPress haben übrigens mitgedacht – <code>/admin</code> funktioniert genauso und leitet zu <samp>/wp-admin</samp> weiter.
    </p>
    <p>
        Sie gelangen zu einer Login-Maske zum Backend. Hier müssen sie sich mit Ihrem Benutzernamen und Ihrem Passwort anmelden. Haben sie das Passwort vergessen können Sie sich an
        Ihre E-Mail-Adresse einen Link zum neu setzen des Passwortes senden lassen.
    </p>
    <h3 id="gui">Die Oberfläche <small class="subhead">Der Aufbau des Backends</small>
    </h3>
    <h4>Die Admin-Bar</h4>
    <p>
        Am Kopf des Backends befindet sich die Admin-Bar. Sie stellt einige QuickLinks zur Verfügung
    </p>
    <ul>
        <li>Seitentitel<br /> Über den Seitentitel gelangen Sie schnell zur Startseite der Homepage – also ins Frontend.
        </li>
        <li>Kommentarverwaltung<br /> Zum Blog System von WordPress gehört auch eine Kommentarfunktion. Hier können neuen Kommentare zur Freischaltung angesehen werden.
        </li>
        <li>+ Neu<br /> Diese Schaltfläche bietet eine einfache Möglichkeit schnell neue Inhalte zu erstellen.
        </li>
        <li>»Willkommen«<br /> hier können sie sich vom Backend abmelden oder Ihr Profil und Ihre Persönlichen Einstellungen bearbeiten
        </li>
    </ul>
    <p>
        Sind Sie im Backend angemeldet und besuchen das Frontend der Seite wird standardmäßig auch her die Admin-Bar angezeigt. Der Seitentitel führt dann ins Backend und bietet
        zusätzliche Quicklinks zu häufig benötigten Seiten. <small>Die Adminbar im Frontend kann im persönlichen Profil deaktiviert werden.</small>
    </p>
    <h4>Das Admin-Menü</h4>
    <p>
        Auf der linken Seite finden Sie das Admin-Menü. Über dieses Menü gelangen Sie zu allen Funktionen, für die Sie freigeschaltet sind. Für den Hauptmenüpunkt in dem Sie sich
        momentan befinden werden die Unterpunkte direkt angezeigt, für die anderen erreichen Sie die Unterpunkte in dem Sie mit der Maus über den Menüpunkt fahren.
    </p>
    <h4>Der Inhaltsbereich</h4>
    <p>
        In dem großen Bereich neben dem Menü gibt es je nachdem woe Sie sich grade befinden die entsprechenden Inhalte zu sehen. Das kann zum Beispiel die
        <a href="#bearbeitungsansicht" title="">Bearbeitungsansicht</a> für Beiträge oder Seiten sein, eine Liste aller Kommentare oder Ihre Profileinstellungen.
    </p>
    <p>
        Wenn Sie sich gerade eingeloggt haben sehen Sie hier das <strong>Dashboard</strong>. Es bietet Ihnen einen schnellen Überblick über die Homepage.
    </p>
    <p>
        Oben rechts finden Sie auf den meisten Seiten die Schaltflächen <em>Optionen</em> und <em>Hilfe</em>. In den Optionen können Sie die Darstellung der aktuellen Seite
        anpassen, die Hilfe bietet zusätzliche Informationen und Anleitungen.
    </p>
    <blockquote class="small">
        Ihnen gefällt das grau-blaue nicht? Gehen Sie doch mal in Ihre Profileinstellungen und probieren sie die anderen Farbschemata aus.
    </blockquote>
    <h3 id="bearbeitungsansicht">Die Bearbeitungsansicht <small class="subhead">Inhalte erstellen und ändern</small>
    </h3>
    <p>
        Ich möchte an dieser Stelle einmal allgemein auf die Bearbeitungsansicht für Beiträge und Seiten eingehen, da Sie einen der wichtigsten Bestandteile bildet. Ein Großteil
        der Inhalte der Homepage wird in dieser Ansicht bearbeitet und dabei gilt es einige Dinge zu beachten. Daher möchte ich mich vor Allem bei der Beschreibung des Editors
        nicht kurz fassen. ;)
    </p>
    <p class="small">
        <em>Möchten Sie die Bearbeitungsansicht sehen während Sie diesen Abschnitt lesen, fahren Sie zunächst mit dem Abschnitt <a href="#beitraege" title="">Beiträge</a> fort und
            kehren später hierhin zurück. Lesen Sie diesen Abschnitt aber auf jeden Fall sorgfältig durch, insbesondere die Beschreibung des <a href="#editor" title="">Editors</a>.</em>
    </p>
    <p class="small">
        <em>Ich rede in diesem Abschnitt der Einfachheit halber nur von Beiträgen, gemeint sind aber immer sowohl Beiträge wie auch seiten und – wenn vorhanden – benutzerdefinierte
            Beitragstypen (Custom Post Types).</em>
    </p>
    <h4>Der Kopfbereich</h4>
    <p>
        Zu oberst befindet sich ein Feld für den Titel des Beitrags. Der Titel des Beitrags wird in den meisten Themes im Frontend als große Hauptüberschrift angezeigt. Der Titel
        im Menü kann ein anderer sein.
    </p>
    <p>
        Darunter finden Sie den sog. Permalink, also den Link unter dem der Beitrag in seiner Einzelansicht oder die Seite angesehen werden kann. Ebenso gibt es hier einen Button
        <em>Beitrag ansehen</em> mit dem Sie den Beitrag direkt im Frontend ansehen können. Es ist auch möglich sich einen Kurzlink anzeigen zu lassen. Dies ist günstig für das
        Teilen eines bestimmten Beitrags via E-Mail oder in sozialen Netzwerken.
    </p>
    <!-- Editor ------------ -->
    <h4 id="editor">Der Editor <small class="subhead">Das Herzstück der Bearbeitung</small>
    </h4>
    <p>
        Unterhalb finden Sie den Editor für den Inhalt des Beitrags.
    </p>
    <p class="small">
        Standardmäßig ist die visuelle Darstellung des Editors aktiv. Mit der Schaltfläche <em>Text</em> über dem Editor können Sie auf eine reine Textansicht umschalten. In dieser
        Ansicht kann der HTML-Code direkt bearbeitet werden, daher sollten hier grundlegende HTML-Kenntnisse vorhanden sein. Ich beschränke mich in dieser Anleitung auf den
        visuellen Editor.
    </p>
    <p>
        Im großen Textbereich können Sie den Text sie den Text der Seite schreiben, wie Sie es aus Textverarbeitungsprogrammen (z.B. Microsoft Word) gewohnt sind. Über die
        Werkzeugleiste oben kann der Text formatiert werden. Hierbei sind einige Dinge zu beachten. Dazu aber später mehr.
    </p>
    <p>
        Einen <strong>Absatz</strong> fügen Sie durch betätigen der Taste <code>&crarr;</code> in den Text ein. Absätze haben meist einen gewissen Abstand zum nächsten Absatz. <em>Bitte
            fügen Sie keine Leerzeile zwischen die Absätze ein, der Abstand entsteht automatisch und ist im <a href="#theme" title="">Theme definiert.</a></em> Möchten Sie einen
        <strong>Zeilenumbruch</strong> ohne Abstand erzielen, erreichen Sie das über <code>&uArr;</code> + <code>&crarr;</code>.
    </p>
    <p>
        Absätze und Zeilenumbrüche dienen der inhaltlichen Strukturierung des Textes in Gedankenabschnitte. Echte Zeilenumbrüche sind extrem selten. Bitte fügen Sie keinen
        Zeilenumbruch ein damit z.B. der nächste Satz in der nächsten Zeile beginnt oder der Text unterhalb eines Bildes weitergeht und es keinen inhaltlichen Grund dafür gibt. Die
        Breite des Editors ist möglicherweise sehr unterschiedlich zu der des Inhaltsbereichs im Frontend und selbst diese variiert je nach Ausgabegerät oder Größe des
        Browserfensters.
    </p>
    <p>
        In der Werkzeugleiste gibt es einige Schaltflächen um den Text zu formatieren. Ist die zweite Zeile der Werkzeugleiste nicht sichtbar, kann Sie über die Schaltfläche <em>Werkzeugleiste
            umschalten</em> in der ersten Zeile ganz rechts eingeblendet werden. Möglicherweise sind nicht alle der im Screenshot sichtbaren Formatierungsmöglichkeiten verfügbar.
    </p>
    <p>
        <strong>Ein sehr wichtiges Werkzeug sind die Absatz- und Zeichenformate</strong>.
    </p>
    <p>
        Formate werden im Theme definiert. Mit ihnen können immer wieder benötigte Textformatierungen leicht einheitlich vorgenommen werden. Werden die Formate durchgehend auf der
        kompletten Homepage verwendet ist es ein leichtes die Hervorhebung auf der kompletten Homepage zu ändern. Es muss lediglich im Theme die Definition angepasst werden. Wurde
        keine Formate verwendet und die Formatierungen manuell vorgenommen muss jede Stelle einzel angefasst und geändert werden. Bei 2 Seiten oder Beiträgen ist das noch machbar,
        aber es kann sehr schnell zu einer unnötigen Fleißarbeit ausarten. Daher meine Bitte: Wann immer Sie eine Formatierung öfter vornehmen möchten und sei es nur das
        Hervorheben eines bestimmten Wortes, bitten Sie den Theme-Entwickler oder Administrator ein Format hinzufügen. Sehr wichtige Absatzformate sind zum Beispiel die
        Überschriften.
    </p>
    <p>
        Die Definition von Formaten beeinflussen die Darstellung des Textes. Wurden beispielsweise alle Telefonnummern mit dem Absatzformat <em>Telefon</em> markiert kann denn
        Nummern auf der Seite automatisch ein Icon vorangestellt werden.<br /> <span class="glyphicon glyphicon-earphone"></span>0123 4567890
    </p>
    <p>
        Ein Absatzformat gilt für einen gesamten Absatz und ein Absatz kann auch nur ein Absatzformat haben. Um ein Absatzformat auf einen Absatz anzuwenden oder das Format des
        Absatzes zu ändern, platzieren Sie den Cursor (Schreibmarke) im Absatz. In der Absatzformat-Liste sehen Sie jetzt das aktive Format. Mit einem Klick darauf öffnet sich die
        Liste und Sie können ein anderes Format auswählen.
    </p>
    <p>
        Zeichenformate können für einen kompletten Absatz gelten oder nur für die Zeichen, die mit Ihnen versehen wurden. Es können mehrere Zeichenformate auf den selben Text
        angewendet werden. Um zu sehen welche Formate für den Text an der Stelle des Cursors oder den markierten Text aktiv sind, klicken Sie auf die Schaltfläche <em>Formate</em>.
        Es wird nun eine Liste aller definierten Formate angezeigt. Aktive Formate sind durch einen Balken links neben dem Formatnamen markiert. Mit einem Klick auf eines der
        Formate fügen Sie das Absatzformat hinzu oder entfernen es.
    </p>
    <p class="lead">
        Verwenden Sie wann immer möglich die vordefinierten Formate und Werkzeuge. Nur so kann ein einheitliches Erscheinungsbild der Homepage gewährleistet werden und auch für
        technische Aspekte spielt das eine Rolle.
    </p>
    <p>
        Über die Werkzeuge können Sie zum Beispiel einige Wörter <strong>fett</strong> oder <em>kursiv</em> setzen, oder auch einen ganzen Absatz als Zitat markieren. <em>Verwenden
            Sie ein passendes vordefiniertes Absatz- und Zeichenformat wenn vorhanden.</em>
    </p>
    <p>
        <strong>Markieren Sie Überschriften und Zwischenüberschriften unbedingt mit den entsprechenden Absatzformaten.</strong><br /> Das ist vor allem für Suchmaschinen und
        Screenreader wichtig (siehe <a href="#barrierefreiheit" title="">Exkurs Barrierefreiheit</a>), aber auch für die einheitliche Darstellung der Überschriften auf der
        Homepage. Setzen Sie Überschriften nicht zusätzlich fett. Das wird vom Absatzformat automatisch übernommen. Es gibt auch Themes in denen die Überschriften nicht Fett
        dargestellt werden sollen. Ein manuelles setzten auf fett würde das überschreiben und der Entwickler und Designer des Themes hat sich bestimmt was bei seiner Wahl gedacht.
        ;)
    </p>
    <p>
        In der Einzelbeitragsansicht wird der Beitragstitel in der Regel als <em>Überschrift 1</em> gesetzt. Daher sollten Sie für Zwischenüberschriften die Formate <em>Überschrift
            2</em> und höher verwenden.
    </p>
    <p>
        Verwenden Sie für <strong>Aufzählungs- und nummerierte Listen</strong> die entsprechenden Werkzeuge in der Werkzeugleiste.
    </p>
    <p>
        Vorhandene Texte können über Copy & Paste (Kopieren und Einfügen) eingefügt werden. Der Editor wird versuchen die Texte von nicht benötigten Steuerzeichen zu bereinigen.
        Sollen alle Formatierungen des Textes beim Einfügen entfernt werden können Sie die Schaltfläche <em>Als Text einfügen</em> aktivieren. Beim kopieren von Texten von anderen
        Homepages ist besondere Vorsicht geboten. Es kann passieren, dass unsichtbare Elemente mit kopiert werden, die auf der eigenen Homepage andere Auswirkungen haben. In einem
        solchen Fall können Nutzer mit HTML-Kenntnissen dieses in der Text-Ansicht entfernen. Andernfalls kann es helfen, die Texte zunächst in einem Textverarbeitungsprogramm
        (z.B. Microsoft Word) einzufügen und dann von dort aus erneut zu kopieren und mit aktivierter <em>Als Text einfügen</em> Schaltfläche erneut einzufügen. Führt auch das
        nicht zum gewünschten Ergebnis, melden Sie sich bitte beim Administrator.
    </p>
    <h5>Bilder hinzufügen</h5>
    <p>
        Bilder bzw. Medien sind ein eigenes, etwas größeres Kapitel und werden daher auch in dieser Anleitung in einem <a href="#medien" title="">eigenen Abschnitt</a> behandelt.
        Dort wird auch erklärt wie Bilder korrekt hochgeladen und in einen Beitrag eingefügt werden.<br /> <a href="#medien" title="">Zum Abschnitt Medien</a>
    </p>
    <h4>Das Außenrum</h4>
    <p>
        Neben und unter dem Editor befinden sich einige Boxen für die Eingabe von Metadaten. Diese Unterscheiden sich je nach Beitragstyp (Beitrag oder Seite).
    </p>
    <p>
        Die Wichtigste, die bei allen Beitragstypen sehr ähnlich ist, ist ganz oben neben dem Titel und Editor zu finden: <strong><em>Veröffentlichen</em></strong>. Sie beinhaltet
        den Status und die Sichtbarkeit des Beitrags und vor Allem einen Button zum <em>Veröffentlichen</em> bzw. <em> Aktualisieren des Beitrags.</em> Über diesen Button werden
        die Änderungen gespeichert und sind dann im Frontend sichtbar.
    </p>
    <p>
        Eine weitere Box die bei allen Beitragstypen angezeigt wird, oder werden kann, ist die Box zur Festlegung des <strong><em>Beitragsbildes</em></strong>. Auf die Funktion der
        <a href="#medien-beitragsbilder" title="">Beitragsbilder</a> gehe ich in einem <a href="#medien-beitragsbilder" title="">eigenen Abschnitt</a> ein.
    </p>
    <p class="small">
        Auf die anderen MetaBoxen wird in den entsprechenden Abschnitten bei den Beiträgen und Seiten eingegangen.
    </p>
    <ul class="small">
        <li><a href="#beitr-bearbeiten" title="">Beiträge bearbeiten</a></li>
        <li><a href="#seiten-bearbeiten" title="">Seiten bearbeiten</a></li>
    </ul>
</section>