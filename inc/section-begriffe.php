<section id="begriffe">
    <h2>Das Grundvokabular <small class="subhead">Ein paar Begriffe zu Beginn</small>
    </h2>
    <h3 id="frontend">Frontend – Backend <small class="subhead">Besucher – Administrator</small>
    </h3>
    <p>
        <em>Kurz gesagt: das Frontend sieht der Besucher der Seite, im Backend tobt sich der Administrator aus.</em>
    </p>
    <p>
        Das <strong>Frontend</strong> ist also die Fassade der Homepage, das was Besucher der Seite angezeigt bekommen. Hier werden die Inhalte dargestellt, es gibt ein Menü zur
        Navigation und vielleicht ein Kontaktformular.
    </p>
    <p id="Backend">
        Das <strong>Backend</strong> ist die Administrationsoberfläche des Content Management Systems, also das Gegenstück zum Frontend. Hier werden vor allem die Inhalte der
        Homepage verwaltet und aktualisiert. Zusätzlich können Einstellung getroffen, die Menüs angepasst und zusätzliche Funktionen installiert werden und noch vieles, vieles
        mehr.
    </p>
    <p>
        Das Frontend ist hier eher uninteressant, es soll ja um die Verwaltung der Inhalt gehen und die geschieht nun mal im Backend. Wahrscheinlich werden Sie in Zukunft einen
        Großteil der Zeit nicht im Frontend sondern im Backend verbringen. Daher gibt es auch keine Beschreibung des Frontends, sehr wohl aber ein des <a href="#backend" title="">Backends</a>.<br />
        <small><a href="#backend" title="">Abschnitt Backend</a></small>
    </p>
    <h3 id="beitrag">Beiträge – Seiten – Archive <small class="subhead">Die Inhalte der Homepage</small>
    </h3>
    <p>
        In WordPress wird standardmäßig zwischen zwei Arten von Hauptinhalten unterschieden: Beiträge und Seiten. Zusätzlich können von einem Theme oder Plugin auch weitere
        <a href="#posttypes" title="">Post Types</a> registriert werden, aber dazu <a href="#posttypes" title="">später</a> mehr.
    </p>
    <p>
        Ein <strong>Beitrag</strong> ist quasi ein Artikel. Eine Liste von Artikeln wird oft in sog. Archiven angezeigt. Oft werden Beiträge für die aktuellen und dynamischen
        Inhalte verwendet. Sie können in <a href="#kategorien" title="">Kategorien</a> eingeordnet werden und es können <a href="#tags" title="">Tags</a>, zusätzliche Schlagworte,
        angefügt werden. <small>Mehr dazu im Abschnitt zu den <a href="#metadaten" title="">Metadaten</a>.</small> Beiträge tauchen auf den meisten Homepages nicht im Menü auf und
        sind daher nur über die Archive zu erreichen.
    </p>
    <p id="seite">
        Eine <strong>Seite</strong> ist dagegen eine eher statische Inhaltsform. Dies kann zum Beispiel eine »Über Uns«-Seite sein oder eine Auflistung der Leistungen. Das
        Impressum und eine Kontaktseite sind weitere klassische Beispiele. Seiten können hierarchisch sortiert werden. Oft werden Seiten auch in Menüs angezeigt, damit der Besucher
        schnell darauf zugreifen kann.
    </p>
    <p id="archiv">
        Eine <strong>Archiv</strong> ist eine Auflistung von Beiträgen mit bestimmten Eigenschaften. Bitte nicht falsch verstehen – die Beiträge müssen keineswegs alt sein um in
        einem Archiv dargestellt zu werden. Ein typisches Beispiel wäre ein Bereich »Aktuelles« auf der Homepage, der ein Archiv mit den neuesten Beiträgen darstellt.
    </p>
    <p>
        Die in einem Archiv dargestellten Beiträge können nach verschiedensten Kriterien gewählt werden: <a href="#kategorien" title="">Kategorie</a>, <a href="#tags" title="">Schlagwort
            / Tag</a>, Veröffentlichungsdatum, Autor … Archive werden aber meist von Administratoren erstellt. Sie sollten das nur im Hinterkopf behalten und beim veröffentlichen
        eines Beitrags auch daran denken eine Kategorie auszuwählen. Andernfalls wird der Beitrag eventuell nicht auf der Homepage erscheinen.
    </p>
    <h3 id="media">Medien <small class="subhead">Bilder & Co.</small>
    </h3>
    <p>
        Medien sind in WordPress die weiteren visuellen Inhalte der Homepage. Damit sind vor Allem Bilder gemeint, es können aber auch andere Formate wie Musik, Videos oder
        PDF-Dateien sein. Sie können direkt im Backend hochgeladen und verwaltet werden und werden in Beiträge oder Seiten eingefügt.
    </p>
    <h3 id="theme">Theme <small class="subhead">Die Gestaltung der Seite</small>
    </h3>
    <p>
        Das Theme definiert das Design der Homepage. Es besteht aus verschiedenen Templates für die Darstellung von unterschiedlichen Inhalten (Seiten, Beiträge, Archive mit einer
        übersicht über mehrere Beiträge …). Die Templates enthalten Platzhalter für die Inhalte der Homepage. So gibt es zum Beispiel Platzhalter für Menüs und für die eigentlichen
        Inhalte der Seite.<br /> Ein Theme muss von einem Administrator installiert und aktiviert werden.
    </p>
    <h3 id="sidebar">Sidebar <small class="subhead">Platz neben den Hauptinhalten</small>
    </h3>
    <p>
        Neben den erwähnten Platzhaltern für die eigentlichen Seiteninhalte gibt es auch die sogenannten Sidebars. Eine Sidebar ist ein Bereich der Seite in dem kleine
        Informationseinheiten, meist Widget, platziert werden können. Die Sidebars werden im Theme festgelegt, Ihre Inhalte können unter <em>Design › Widgets</em> verwaltet werden.
        Manche Themes verwenden für verschiedene Templates auch unterschiedlich Sidebar. So kann es zum Beispiel eine Sidebar für die Archive und Einzelansicht Beiträge geben und
        eine andere für die festen Seiten.
    </p>
    <p class="small">
        Eine Sidebar muss aber nicht an der »Seite« sein. Manche Themes besitzen auch Sidebars, die unter dem Inhalts-, im Kopf- oder Fußbereich der Seite platziert sind. In
        WordPress werden alle Bereiche, in den Widgets dargestellt werden können Sidebar genannt.
    </p>
    <h3 id="plugin">Plugin <small class="subhead">So wird WordPress zum Alleskönner</small>
    </h3>
    <p>
        Ein Plugin erweitert die Funktionalität von WordPress. Je nach Plugin kann dies auf unterschiedlichste Weise geschehen. Es können zu den standardmäßigen Seiten und
        Beiträgen weitere Seitentypen hinzugefügt werden, es können bestimmte Komponenten registriert werden … die Möglichkeiten sind sehr vielfältig und hier unmöglich in ihrer
        ganzen Fülle aufzulisten. Nicht umsonst gibt es derzeit über 36.000 frei verfügbare Plugins für WordPress mit den unterschiedlichsten Features.
    </p>
    <p>
        Plugins müssen von einem Administrator installiert werden und aktiviert werden. Dies geschieht unter dem Menüpunkt <em>Plugins</em>. Manche Plugins fügen auch eine
        Einstellungsseite hinzu, die dann meistens unter <em>Einstellungen</em> zu finden ist.
    </p>
    <h3 id="widget">Widget <small class="subhead">Kleine Informationsblöcke</small>
    </h3>
    <p>
        WordPress stellt neben den eigentlichen Hauptinhalten (Seiten und Beiträge) sogenannte Widgets zur Verfügung. Widgets sind Module, die bestimmte Funktionen bieten und
        Ausgaben erzeugen. Sie können sehr einfach per Drag & Drop (Klicken und Ziehen) auf die vorgesehenen Flächen im Theme platziert werden, den sogenannten Sidebars. Ein Widget
        kann zum Beispiel ein Menü beinhalten, ein Suchfenster generieren, oder einfach nur Text darstellen, der auf jeder Seite auftauchen soll.
    </p>
    <p>
        Die aktuell im System registrierten Widgets können unter <em>Design › Widgets</em> eingesehen und einer Sidebar hinzugefügt werden.
    </p>
    <h3 id="shortcode">Shortcode <small class="subhead">Komplexer Code einfach verpackt</small>
    </h3>
    <p>
        Ein Shortcode kann als Platzhalter für komplizierten Code gesehen werden. So kann mit einem Schlüsselwort und der optionalen Angabe von Attributen sehr einfach eine
        komplexere Struktur in einen Beitrag eingefügt werden.
    </p>
    <p>
        Da Shortcodes meist von Plugins oder Themes definiert werden, sind die in Ihrer WordPress-Installation tatsächlich vorhandenen Shortcodes sehr individuell. Fragen Sie im
        Zweifel den Administrator der Seite.
    </p>
    <h3 id="metadaten">MetaDaten <small class="subhead">Was die Inhalte noch so ausmacht</small>
    </h3>
    <p>
        Neben Titel und Inhalt einer Seite oder eines Beitrags werden auch noch andere zu der Seite / dem Beitrag gehörige Daten gespeichert, die sogenannten Metadaten. Dabei
        handelt es sich beispielsweise um den Autor, das Veröffentlichungsdatum, eine <a href="#kategorien" title="">Kategorie</a> oder ein Beitragsbild. Diese Daten können zum
        beispiel genutzt werden um Beiträge zu Filtern und damit eigene Archive zu erstellen – alle Beiträge der Kategorie »Aktuelles« oder alle Artikel von Autor »Lukas Fritze«.
        Die Eingabefelder für die Metadaten befinden sich in WordPress neben oder unter dem Texteditor in der Bearbeitungsansicht in den MetaBoxen.
    </p>
    <p>
        Ein Theme oder Plugin kann zu den standardmäßig vorhanden MetaBoxen, weitere hinzufügen. Die hier eingegebenen Daten werden dann meist genutzt um Einstellungen für die
        Darstellung von Komponenten zu realisieren oder um zu bestimmten Beitragstypen Informationen hinzuzufügen (z.B. Termindaten für Veranstaltungen).
    </p>
    <p class="small">
        Ist eine gesuchte MetaBox nicht vorhanden, kann es sein, dass sie nur auf unsichtbar gestellt ist. Um das zu prüfen gehen sie in der Bearbeitungsansicht rechts oben auf den
        Punkt <em>Optionen</em> und aktivieren das Häkchen für die MetaBox.
    </p>
</section>