<section id="menu">
    <h2>Menüs <small class="subhead">Die Navigation auf der Homepage</small>
    </h2>
    <p>
        Menüs bilden die Navigation im Frontend für den Besucher der Seite. Eine Homepage kann mehrere Menüs an verschieden Stellen besitzen. <small>(Mehr dazu in den
            <a href="#menu-options" title="">Einstellungen</a>.)</small>
    </p>
    <p>
        Alles zu den Menüs finden Sie im Backend unter <strong><em>Design › Menüs</em></strong>.
    </p>
    <p class="small">
        Möglicherweise haben Sie auf Grund Ihrer <a href="#roles" title="">Rolle</a> keinen Zugriff auf die Verwaltung der Menüs. Melden Sie sich in diesem Fall bitte beim
        Administrator der Homepage.
    </p>
    <h3 id="menu-neu">Ein neues Menü erstellen <small class="subhead">Erste Wegweiser</small>
    </h3>
    <p>
        Rufen Sie den Punkt <strong><em>Design › Menüs</em></strong> auf. Beim ersten Aufruf sehen Sie hier automatisch die Funktion zum Erstellen eines Menüs. Ist schon ein Menü
        vorhanden können Sie oben neben der Menüauswahl den Link <em>erstelle ein neues Menü</em> aufrufen.
    </p>
    <p>
        Um ein neues Menü zu erstellen müssen Sie lediglich einen Namen für das Menü angeben und auf <em>Menü erstellen</em> klicken. Der Name des Menüs ist hauptsächlich zu
        Verwaltung mehrerer Menüs im Backend gedacht und wird nur sehr selten im Frontend angezeigt. Sie können hier also einen beliebigen Namen wählen, z.B. »Hauptmenü«.
    </p>
    <p>
        Ein neues Menü erscheint noch nicht im Frontend. Es müssen erst einige <a href="#menu-options" title="">Einstellungen</a> getroffen werden.
    </p>
    <h3 id="menü-bearb">Menüs bearbeiten <small class="subhead">Die Übersicht behalten</small>
    </h3>
    <p>
        Um ein Menü zu bearbeiten wählen Sie es oben in der Menüauswahlliste aus und bestätigen Ihre Wahl mit <em>-Wähle-</em>. Anschließend können Sie den Namen des Menüs ändern,
        die Menüstruktur bearbeiten, sowie einige Einstellungen vornehmen.
    </p>
    <h4>Menüpunkte hinzufügen</h4>
    <p>
        Ein neues Menü enthält zunächst keine Menüpunkte. Diese müssen dem Menü erst hinzugefügt werden. Dazu wählen Sie aus der Box auf der linken Seite die Elemente die
        hinzugefügt werden sollen und fügen die Punkte dann mit <em>Zum Menü hinzufügen</em> in das Menü ein.
    </p>
    <p>
        Standardmäßig wird hier eine Liste der Seiten angezeigt. Sie können aber auch Kategorie-Archive oder andere Elemente dem Menü hinzufügen. Öffnen Sie die entsprechende
        Auswahlliste mit eine Klick auf die Überschrift.
    </p>
    <p class="small">
        Sie vermissen eine Auswahlliste zum hinzufügen von Elementen? Rechts oben unter <em>Optionen</em> Können Sie weitere Listen einblenden.
    </p>
    <h4>Menüstruktur ändern</h4>
    <p>
        Neue Menüpunkte werden unten an das Menü angehängt, Um die Reihenfolge der Menüpunkte zu ändern, können Sie die Elemente einfach per Drag & Drop (Klicken und Ziehen) an die
        gewünschte Position ziehen. Ein gestrichelter Kasten zeigt beim Verschieben die Position an, an der das element beim Loslassen eingefügt wird.
    </p>
    <p>
        Mit dem Verschieben von Elementen ist es auch möglich <em>Unterpunkte</em> zu erstellen. Ziehen Sie die Elemente leicht nach Rechts und unter den gewünschten Hauptpunkt.
        Die Position an der das element platziert wird sehen sie als Vorschau.<br /> Alternativ können Sie das Element auch in den Menüpunktoptionen verschieben.
    </p>
    <p>
        Vergessen Sie nicht die Änderungen mit einem Klick auf <em>Menü speichern</em> zu übernehmen.
    </p>
    <h4>Optionen für Menüpunkte</h4>
    <p>
        Für einen einzelnen Menüpunkt können optional noch weitere Konfigurationen vorgenommen werden. Die Optionen können sie über einen Klick auf den Pfeil auf der rechten Seite
        des Elements einblenden.
    </p>
    <p>
        Hier können Sie den im Menü angezeigten Namen ändern. Dieser ist vorbelegt mit dem namen der Seite oder Kategorie (oder je nachdem was das für ein Menüpunkt ist). So ist es
        möglich einen zu langen Seitentitel im Menü zu verkürzen ohne die Hauptüberschrift, also den Titel der Seite, ändern zu müssen. Den ursprünglichen Titel des Menüpunkts
        sehen Sie weiter unten.
    </p>
    <p>
        Zudem gibt es hier einige Möglichkeiten zum Verschieben des Elements. Diese unterscheiden sich je nach aktueller Position des Elements.
    </p>
    <h3 id="menu-options">Menü Einstellungen <small class="subhead">irgendwas</small>
    </h3>
    <p>
        Unterhalb der Menüstruktur finden Sie einige Einstellungen für das Menü.
    </p>
    <p>
        Ist der Hacken für <em>Seiten automatisch hinzufügen</em> gesetzt, werden neue Seiten automatisch an das Menü angefügt. Bitte beachten Sie, dass dies jedoch nur für Seiten
        der ersten Ebene gilt. Ebenen der zweiten oder tieferen Ebenen müssen manuell hinzugefügt werden.
    </p>
    <p>
        In einem Theme können mehrere Positionen für Menüs definiert werden. An welcher Stelle das Menü angezeigt werden soll können Sie hier festlegen. Ein Menü kann an mehreren
        Stellen angezeigt werden, aber eine Menüposition kann nur ein Menü enthalten. Daher sehen Sie hinter der Menüposition welches Menü aktuell für diese Position aktiviert ist.
        Wählen Sie dann diese Position aus wird der Wert überschrieben und das andere Menü nicht mehr an dieser Position angezeigt.
    </p>
    <p>
        Vergessen Sie nicht die Änderungen mit einem Klick auf <em>Menü speichern</em> zu übernehmen.
    </p>
    <h4>Positionen verwalten</h4>
    <p>
        Existieren in Ihrem Theme mehrere Menüposition kann es hilfreich sein alle Positionen auf einmal zu organisieren. Dazu finden Sie bei den Menüs ganz oben den Tab <em>Positionen
            verwalten</em>.
    </p>
    <p>
        Hier sehen Sie eine Übersicht aller Menüpositionen im Theme und können die Zuweisung ändern. außerdem gibt es nützliche Links direkt zur Bearbeitung der zugewiesenen Menüs.
    </p>
</section>