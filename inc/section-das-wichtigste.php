<section id="das-wichtigste">
    <h2>
        Das Wichtigste <small class="subhead">Die Basics auf einen Blick</small>
    </h2>
    <p>
        In diesem Kapitel finden Sie eine kurze Zusammenfassung der wichtigsten Punkten zur Inhaltspflege.
    </p>
    <p class="small">
        <em>Sie haben keinen Schimmer von was ich hier rede? Überspringen Sie dieses Kapitel und starten Sie mit den <a href="#begriffe">Grundbegriffen.</a></em>
    </p>
    <p class="small">
        <strong>Stichwort Barrierefreiheit:</strong><br> Stellen Sie sich vor Sie können nicht oder nur sehr schwer sehen. Man muss ja nicht komplett blind sein, aber schon eine
        Sehschwäche genügt, dass man auf Hilfsmittel angewiesen ist. Bei vielen reicht hier eine Brille, aber einige brauchen weitere Hilfsmittel, wie größeren Text oder Programme,
        die ihnen die Inhalte einer Website vorlesen. Solche Programme nennt man Screenreader. Viele der Hinweise hier verbessern die Barrierefreiheit und sorgen so dafür, dass das
        Internet für alle Menschen gleichermaßen nutzbar wird. Aber auch technisch gesehen ist Barrierefreiheit ein wichtiger Aspekt, da er allgemein die Qualität einer Website
        verbessert.
    </p>
    <h3 id="wichtig-formate">
        Formate <small class="subhead">Überschriften, Absätze, Listen, …</small>
    </h3>
    <p>
        <strong>Verwenden Sie wann immer möglich die vordefinierten Formate und Werkzeuge.</strong> Nur so kann ein einheitliches Erscheinungsbild der Homepage gewährleistet werden
        und auch für technische Aspekte spielt das eine Rolle. Mehr zu diesem Thema im Abschnitt <a href="#editor">Editor</a>.
    </p>
    <h4>Überschriften</h4>
    <p>
        <strong>Missbrauchen Sie Überschriften nicht zu Formatierungszwecken.</strong>
    </p>
    <p>
        Eine Überschrift hat eine Bedeutung. Sie ist technisch gesehen was besonderes und auch der Entwickler hat sich was dabei gedacht, warum Überschriften so aussehen und der
        „normale“ Text anders. Denke sie an ein Sachbuch. Alles was im Inhaltsverzeichnis stehen würde ist eine Überschrift, der Rest nicht.
    </p>
    <p>
        Hervorhebungen können Sie mit den Markierungen für <strong>fetten</strong> <small>(bold)</small> oder <i>kursiven</i> <small>(italic)</small> Text vornehmen. Farbiger Text
        hat im Internet meist die Bedeutung eines Links und sollte daher vermieden werden, wenn sich um keinen Link oder keine Überschrift handelt.
    </p>
    <p class="small">
        Tatsächlich erstellen Browser und Suchmaschinen im Hintergrund genau ein solches Inhaltsverzeichnis. Nutzer die auf Hilfsmittel angewiesen sind nutzen die Hierarchie der
        Überschriften, um sich überhaupt auf der Seite orientieren zu können. Sie sehen kein Schriftgrößen, -stärken, oder farben.
    </p>
    <h4>Listen</h4>
    <p>
        <strong>Markieren Sie eine Listen immer als Liste. Verwenden Sie keine normalen Absätze für Listen.</strong>
    </p>
    <p>
        So ist sie auch technische eine echte Liste. Dadurch werden auch wenn die Zeilen kürzer werden und so Zeilenumbrüche entstehen <small>(wie zum Beispiel auf einem
            Smartphone)</small> die Einrückungen ordentlich angezeigt. Außerdem werden Listen so immer einheitlich angezeigt und nicht mal mit einem - und mal mit einem • als
        Aufzählungszeichen Aufzählung. Zudem gibt es andernfalls auch Probleme für Menschen, die auf Hilfsmittel angewiesen sind. <small>Stichwort Barrierefreiheit.</small>
    </p>
    <p>
        Also einfach den Text der Liste markieren und in der Werkzeugleiste als Liste definieren.
    </p>
    <h4>Absätze und Zeilenumbrüche</h4>
    <p>
        <strong>Verwenden Sie Zeilenumbrüche oder Absätze niemals zu Layoutzwecken, sonder nur wenn es eine inhaltliche Begründung gibt.</strong>
    </p>
    <p>
        Bitte fügen Sie keinen Zeilenumbruch ein damit z.B. der nächste Satz in der nächsten Zeile beginnt oder der Text unterhalb eines Bildes weitergeht. Wenn es keinen
        inhaltlichen Grund dafür gibt ist ein oder mehrere Absätze das falsche Mittel. Die Breite des Editors ist möglicherweise sehr unterschiedlich zu der des Inhaltsbereichs im
        Frontend und selbst diese variiert je nach Ausgabegerät oder Größe des Browserfensters.
    </p>
    <p>
        Stellen Sie sich eine Seite mit 3 Leerzeilen auf einem Computer vor. Hier funktioniert das vielleicht noch, dass der Text wie gewollt neben dem Bild steht und danach
        darunter fortgesetzt wird. Aber schon wenn sie die Seite auf einem Tablet besuchen, passt der Text nicht mehr so neben das Bild und es entsteht ein Lücke. Auf einem
        Smartphone passt vielleicht gar kein Text mehr neben das Bild und die Leerzeilen führen zu einem riesigen, unschönen Loch.
    </p>
    <p>
        Wenn Sie der Meinung sind, dass der Abstand vergrößert werden sollte oder Sie einen Abstandshalter benötigen, wenden Sie sich an den Entwickler. Er hilft Ihnen bei einer
        technisch guten Lösung die auf allen Geräten funktioniert.
    </p>

    <h3 id="wichtig-medien">
        Medien <small>Bilder & Co.</small>
    </h3>

    <h4>Text in Bildern</h4>
    <p>
        <strong>Text in Bildern existiert auf der Seite nicht. Binden Sie Text immer als Text ein, niemals als Bild.</strong>
    </p>
    <p>
        Text, der lediglich als Teil eines Bildes auf der Website eingebunden wird, ist technisch gesehen nicht vorhanden. Er wird also nicht von Suchmaschinen gefunden und kann
        auch von normalen Benutzern nicht normal gelesen oder kopiert werden. Insbesondere, wenn man die seite mit einem kleineren Endgerät betrachten wird das reine Lesen zu einer
        echten Herausforderung. Für Nutzer, die auf Hilfsmittel angewiesen sind existiert die Information gar nicht. Sie schließen solche Personen damit faktisch von Ihrer Homepage
        aus.
    </p>
    <p>
        Wenn Sie also beispielsweise einen Flyer auf die Website stellen möchten, binden Sie die PDF-Datei als Link ein und setzen die Informationen, die auf dem Flyer stehen, auch
        als Text auf die Seite. Die Abbildungen, wie Logo oder Bilder können Sie zusätzlich mit einfügen.
    </p>
    <p class="small">
        <i>Warum eine PDF-Datei verlinken und kein Bild?</i> In einem Bild sind keinerlei inhaltliche Informationen enthalten – es besteht lediglich aus vielen kleinen farbigen
        Punkten, vielleicht haben Sie ja den Begriff Pixel schonmal gehört. In einer PDF-Datei existiert der Text noch als Text und ist so auch vergrößert noch gut lesbar. Außerdem
        können ihn so auch Programme, wie zum Beispiel sog. Screenreader, also Programme die den Text für sehbehinderte Menschen vorlesen können, verstehen.
    </p>

    <h4>Beschreibungen von Bildern</h4>
    <p>
        <strong>Beschreiben Sie die Bilder auf der Website für Menschen mit Sehbehinderungen.<br><small>Und Suchmaschinen.</small></strong>
    </p>
    <p>
        Wie oben im kurzen Absatz zum Stichwort Barrierefreiheit beschrieben, gibt es im Internet Nutzer, die auf Hilfsmittel angewiesen sind. Für diese Nutzer existieren Bilder
        quasi nicht. Daher sollten Sie Bilder immer mit einem sog. alternativen Text versehen. Dieser wird angezeigt, wenn das Bild nicht angezeigt werden kann, und wird von
        Screenreadern genutzt. In dieser Beschreibung sollten Sie kurz erläutern was auf dem Bild zu sehen ist. Mit einer solchen Beschreibung kann auch ein eingeschränkter Nutzer
        das Bild verstehen. Ein paar Beispiele:
    </p>
    <ul>
        <li>„Ein fröhliches Kind beim Spielen mit Holzklötzen.“</li>
        <li>„Drei Personen an einem Tisch mit Laptops.“</li>
        <li>„Eine Kirche im gotischen Stil umgeben von grünen Bäumen und einem Friedhof.“</li>
    </ul>
    <p class="small">
        <strong>Tipp:</strong> Halten Sie die Beschreibung kurz und konzentrieren Sie sich auf das Wesentliche. Es ist z.B. nicht interessant, dass das Kind ein gelbes T-Shirt mit
        einem Aufdruck trägt oder dass der Tisch in dem Besprechungsraum aus Buche gefertigt wurde. Genauso wenig interessiert es, dass die Kirche 12 Fenster hat … außer natürlich
        wir sind auf einer Seite, die sich detailliert mit der Architektur der Kirche beschäftigt und das ist <em>die</em> Besonderheit, die mit diesem Bild erläutert werden soll.
    </p>

</section>