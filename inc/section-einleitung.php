<section id="einleitung">
    <h2>Einleitung</h2>
    <h3>WordPress <small class="subhead">ein CMS</small>
    </h3>
    <p>
        WordPress ist eines der am weitesten verbreiteten Content Management Systeme. Aber mal langsam. Was ist eigentlich ein Content Management System?
    </p>
    <p>
        Ein Content Management System ist eigentlich nur ein Ansammlung von Dateien. Werden diese Dateien allerdings auf einem Webserver abgelegt bilden sie zusammen eine Software,
        die das Verwalten der Inhalte einer Homepage ermöglicht. So ist es auch einem durchschnittlichen Computernutzer möglich, die Inhalte einer Homepage zu bearbeiten, zu
        erweitern und so immer auf einem aktuellen Stand zu halten.
    </p>
    <p>
        In einem Content Management System wie WordPress werden die Inhalte getrennt von der Gestaltung in einer Datenbank gespeichert. Dadurch wird es möglich, die Darstellung der
        Inhalte anzupassen ohne die Inhalte anzufassen. Auf der anderen Seite ist es möglich die Inhalte in einem einfachen Editor zu bearbeiten.
    </p>
    <p>
        Ich möchte Ihnen hier eine leicht verständliche Anleitung der grundlegenden Funktionen bieten, sodass Sie als Seiteninhaber die Inhalte eigenhändig verwalten können. Da ich
        täglich mit WordPress arbeite, kann es sein, dass mir die angestrebte Verständlichkeit nicht immer gelingt. Sollte Ihnen also irgendetwas spanisch vorkommen oder wenn sie
        Verbesserungsvorschläge haben, scheuen Sie sich nicht mir eine E-Mail an <a href="mailto:web@fritzedesign.de" title="Mail an Lukas Fritze">web@fritzedesign.de</a> zu
        schreiben.
    </p>
    <p class="small">
        <strong>Für erfahrenere Nutzer:</strong> Ich pflege dieses Projekt in einem <a href="https://gitlab.com/fritzedesign/wordpress-tutorial">Repository auf Gitlab</a>. Wer
        möchte kann auch dort direkt ein sog. Issue erstellen, also einen Fehler melden oder einen Verbesserungsvorschlag machen. Dies erleichtert mir die Übersicht. Und wer sogar
        mit HTML und GIT umgehen kann, darf natürlich auch einen Merge-Request <small>(oder wie github es nennt „Pull Request“)</small> erstellen.
    </p>
    <p class="small">
        <em> Bitte beachten Sie, dass sich dieses Tutorial noch im Aufbau befindet. Es ist daher noch nicht alles perfekt oder vollständig, es gibt noch nicht zu jedem Abschnitt
            Screenshots. Ich werde nach und nach die Kapitel erweitern und verbessern. </em>
    </p>
</section>