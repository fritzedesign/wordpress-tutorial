<section id="seiten">
    <h2>Seiten verwalten <small class="subhead">bearbeiten, erstellen, organisieren</small>
    </h2>
    <p>
        Seiten sind die eher statischen Inhalte der Homepage. Trotzdem – oder gerade deshalb – sollten Sie immer auf einem aktuellen Stand gehalten werden. Seiten sind zudem meist
        im Menü vorhanden und können so direkt angesteuert werden.
    </p>
    <h4>Seitenhierarchie <small class="subhead">Vater, Mutter, Kind</small>
    </h4>
    <p>
        Eine Besonderheit der Seiten ist, dass ihnen eine Elternseite zugewiesen werden kann. Damit ist es möglich eine Seitenhierarchie mit Eltern- und Kindseiten aufzubauen.
        Diese Hierarchie kann beispielsweise genutzt werden um eine Seite mit einer Liste aller ihrer Kindseiten anzuzeigen. Oft wird diese Hierarchie auch im Menü widergespiegelt,
        was aber nicht zwingend erforderlich ist.
    </p>
    <h3 id="seiten-list">Seitenliste <small class="subhead">Eine Übersicht über die Seiten</small>
    </h3>
    <p>
        Wer sich in der Beitragsliste schon zurechtgefunden hat wird hier wenig Problem haben. Auch hier gibt es die Seitenaufteilung, die Filteroptionen und natürlich die große
        Liste – diesmal mit den Seiten.<br /> Kindseiten werden unter Ihren Elternseiten angezeigt und eingerückt.
    </p>
    <p>
        In der Liste sind auch hier die weiteren Funktionen versteckt., Das Filtern dre Liste mit einem Klick auf den Autor funktioniert auch.
    </p>
    <ul>
        <li><em>Bearbeiten</em><br /> Zur Bearbeitungsansicht für die Seite. <small>Ein Klick auf den Seitentitel klappt auch.</small>
        </li>
        <li><em>QuickEdit</em><br /> Öffnet die <a href="#quickedit" title="">QuickEdit-Funktionen</a>.
        </li>
        <li><em>Papierkorb</em><br /> Verschiebt die Seite in den Papierkorb. <small>Um eine Seite endgültig zu löschen muss der Papierkorb geleert oder die Seite aus dem
                Papierkorb entfernt werden</small> .
        </li>
        <li><em>Anschauen</em><br /> Zeigt die Seite im Frontend.
        </li>
    </ul>
    <p class="small">
        Sie haben eine Seite gelöscht? Vergessen Sie nicht auch den Menüpunkt zu entfernen.
    </p>
    <h3 id="seiten-quickedit">QuickEdit <small class="subhead">Schnell die MetaDaten ändern</small>
    </h3>
    <p>
        Wie bei den Beiträgen gibt es auch für die Seiten eine QuickEdit-Funktion, lediglich die Auswahloptionen sind unterschiedlich. Schließlich müssen jetzt ja auch die
        MetaDaten der Seiten bearbeitet werden.
    </p>
    <p>
        Befinden Sie sich in der Seitenliste können Sie die <em>QuickEdit</em>-Änderungen aufrufen, in dem Sie mit der Maus über die zu ändernden Seite fahren und den Punkt <em>QuickEdit</em>
        aufrufen. Es erscheinen nun einige Eingabefelder zum Ändern der Metadaten.
    </p>
    <p>
        Auf der Linken Seite können sie den <em>Titel</em> der Seite und den <em>Permalink</em> ändern. Der Permalink ist der Seitentitel wie er in der URL angezeigt wird. Außerdem
        können Sie hier das <em>Veröffentlichungsdatum</em> verändern und die Seite mit einem <em>Passwort</em> schützen oder ihren Status auf <em>privat</em> setzen, sodass sie
        nur von berechtigten Nutzer gelesen werden kann.
    </p>
    <p>
        Rechts können Sie z.B. eine <em>Elternseite</em> festlegen, ein <em>Seitentemplate</em> wählen oder den <em>Veröffentlichungsstatus</em> ändern.
    </p>
    <p>
        Auch hier daran denken die Änderungen mit dem Button <em>Aktualisieren</em> zu bestätigen und mit <em>Übernehmen</em> zu speichern.
    </p>
    <h3 id="seiten-massenbearbeitung">Massenbearbeitung <small class="subhead">Viele Seiten auf einmal ändern</small>
    </h3>
    <p>
        Die Massenbearbeitung funktioniert natürlich auch für Seiten und nicht nur für Beiträge. Einfach die zu ändernden Seiten auswählen, im Auswahlmenü <em>Aktion wählen</em>
        den Punk <em>bearbeiten</em> aktivieren und auf <em>Übernehmen</em> klicken. Schon erscheinen die Auswahlmöglichkeiten.
    </p>
    <p>
        Links sehen Sie eine Liste der ausgewählten Seiten. Mit einem Klick auf das × können Sie Seiten wieder aus der Auswahl ausschließen. Rechts können Sie den Autor, die
        Elternseite und andere Eigenschaften für alle ausgewählten Seiten festlegen.
    </p>
    <p>
        Mit <em>Aktualisieren</em> speichern Sie die Änderungen ab.
    </p>
    <h3 id="seiten-erstellen">Eine Seite erstellen <small class="subhead">Dauerhafte Inhalte hinzufügen</small>
    </h3>
    <p>
        Und auch hier wieder große Ähnlichkeiten zu den Beiträgen. Es gibt mehrere Stellen an denen man die Funktion zum Erstellen einer neuen Seite aufrufen kann. Alle führen im
        Endeffekt zum selben Ergebnis: man landet in der Bearbeitungsansicht für die neue Seite.
    </p>
    <ul>
        <li><strong>Im Menü</strong><br /> Im Admin-Menü gibt es den Punkt <em>Seiten › Erstellen</em>. <small>Befindet man sich bereits bei den Seiten ist der Menüpunkt sichtbar,
                ansonsten erscheint er, wenn man die Maus über den Punkt <em>Seiten</em> bewegt. </small>
        </li>
        <li><strong>In der Admin-Bar</strong><br /> In der Admin-Bar findet man unter <em>+ Neu</em> einen Punkt <em>Seite</em>.
        </li>
        <li><strong>Neben der Überschrift</strong><br /> Befindet man sich in der Seitenliste oder in der Bearbeitungsansicht einer Seite gibt es oben neben der Überschrift einen
            Button <em>Erstellen</em>.
        </li>
    </ul>
    <p>
        Nicht jede Seite erscheint automatisch im Menü der Seite. Wie Sie eine Seite dem Menü hinzufügen erfahren Sie im Abschnitt <a href="#menue-punkt-hinzufuegen" title="">Menüpunkte
            hinzufügen</a>.
    </p>
    <h3 id="seiten-bearbeiten">Eine Seite bearbeiten <small class="subhead">Die Homepage auf aktuellem Stand halten</small>
    </h3>
    <p>
        <em>Bitte Beachten Sie den Abschnitt <strong><a href="#bearbeitungsansicht" title="">Bearbeitungsansicht</a></strong> für allgemeine Informationen, insbesondere die
            Hinweise zum <a href="#editor" title="">Editor</a>. Hier werden nur die Besonderheiten für Seiten behandelt.</em>
    </p>
    <p>
        <em>Bilder bzw. Medien sind ein eigenes, etwas größeres Kapitel und werden daher auch in dieser Anleitung in einem <a href="#medien" title="">eigenen Abschnitt</a>
            behandelt. Dort wird auch erklärt wie Bilder korrekt hochgeladen und in einen Beitrag eingefügt werden. <a href="#medien" title="">Zum Abschnitt Medien.</a></em>
    </p>
    <p>
        Rechts neben dem Hauptbereich der <a href="#bearbeitungsansicht" title="">Bearbeitungsansicht</a>, also dem Feld für den Titel und dem eigentlichen
        <a href="#editor" title="">Editor</a> <small>(werden im <a href="#bearbeitungsansicht" title="">Abschnitt Bearbeitungsansicht</a> ausführlich beschrieben)</small> ,
        befinden sich die MetaBoxen für die Seite. Hier können Sie zusätzliche zur Seite gehörige Daten, sog. MetaDaten angeben und anpassen. Die Box <em>Veröffentlichen</em> ist
        bei allen Beitragstypen gleich und wird daher auch schon im <a href="#bearbeitungsansicht" title="">Abschnitt Bearbeitungsansicht</a> beschrieben. Die anderen MetaBoxen
        sollen hier erklärt werden.
    </p>
    <p>
        Neben den vorgestellten MetaBoxen kann es auch noch weitere Boxen geben. Diese werden dann z.B. von Plugins hinzugefügt und können sehr unterschiedlich sein. Lesen Sie dazu
        bitte die Beschreibung des Plugins.
    </p>
    <h4>Elternseite</h4>
    <p>
        Die Elternseite einer Seite kann in der MetaBox <em>Attribute</em> angegeben werden. Öffnen Sie hierzu das Auswahlfeld <em>Eltern</em> durch einen Klick und wählen in der
        Liste die Elternseite aus.
    </p>
    <h4>Seitentemplate</h4>
    <p>
        Im Theme können verschiedene Seitentemplates definiert werden. Ein einfaches Beispiel wäre ein Template mit einer Sidebar auf der rechten und eines mit einer auf der linken
        Seite. Aber auch komplexere Templates sind möglich. So kann es ein Template geben, das zusätzlich unter dem Inhalt der Seite eine Liste der Kindseiten anzeigt oder eines
        was die neues Beiträge mit auflistet. Welche Templates vorhanden sind hängt ganz vom aktivierten Theme ab.
    </p>
    <p>
        Um ein Template auszuwählen öffnen Sie in der MetaBox <em>Attribute</em> das Auswahlfeld <em>Template</em> und wählen Sie das gewünschte Template.
    </p>
    <p class="small">
        Es gibt das Auswahlfeld <em>Templates</em> nicht? Dann ist im Theme wohl nur das Standardtemplate definiert. Was nicht benötigt wird muss auch nicht angezeigt werden und
        ein Auswahlfeld mit nur einem Element wäre wohl ziemlich sinnlos, oder?
    </p>
</section>