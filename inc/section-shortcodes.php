<section id="shortcodes">
    <h2>Shortcodes</h2>
    <p>
        Shortcodes sind eine weitere Funktion von WordPress, die ich hier zwar nicht ausführlich behandeln möchte, weil sie schon zu den fortgeschritteneren Techniken gehören, die
        aber auch nicht ganz ignoriert werden können. Sie werden im Text der Beiträge oder Seiten verwendet und dienen als Platzhalter für komplexere Code-Strukturen. So wird es
        ermöglicht diese komplexen Strukturen einfach zu nutzen.
    </p>
    <p>
        Ein Shortcode kann z.B. von einem Theme oder Plugin definiert werden. Daher kann ich hier auch nicht detailliert die vorhanden Shortcodes erklären, weil diese je nach Seite
        sehr unterschiedlich sind. Für weitere Informationen beachten Sie die speziellen Hinweise für Ihre Homepage oder fragen Sie einen Administrator der Seite.
    </p>
    <p>
        Ein Shortcode besitzt immer ein eindeutiges Schlüsselwort. Angewendet wird ein Shortcode indem das Schlüsselwort in eckigen Klammern in den Text der Seite oder des
        Beitrages eingefügt wird, also zum Beispiel <code>[schluesselwort]</code>. Das ist die einfachste Verwendung eines Shortcodes.
    </p>
    <p>
        Optional können manchen Shortcodes verschiedene Attribute übergeben werden, die dieser dann verarbeiten kann. Die Attribute sind für jeden Shortcode sehr individuell. Hier
        wieder der Hinweis auf die speziellen Hinweise für Ihre Homepage bzw. den Administrator der Seite.<br /> Ein Beispiel für die Verwendung eines Shortcodes mit Attributen ist
        der standardmäßig vorhandene Shortcode <samp>[gallery]</samp>, dem z.B. die zu verwendenden Bilder und der Link-Typ übergeben werden können.<br /> <code>[gallery
            link="file" ids="9,19,20,22"]</code>
    </p>
</section>