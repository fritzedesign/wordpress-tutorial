<section id="medien">
    <h2>Medien & Bilder <small class="subhead">Die Seite wird lebendig</small>
    </h2>
    <p>
        In WordPress gehören Bilder zu den Medien, Medien sind aber nicht nur Bilder, sondern können auch Audio-, Video- oder PDF-Dateien sein. Ich werde mich hier auf Bilder
        beschränken, da diese die häufigste Form von Medien sind. Medien machen Ihre Texte lebendig – niemand will nur öde Texte lese, vor Allem nicht im Internet. Daher sind
        Medien ein sehr wichtiges Kapitel in der Erstellung von Inhalten in WordPress.
    </p>
    <p>
        Noch ein Wort vorab zu Bildern. In WordPress gibt es 3 vordefinierte Bildgrößen: <em>Thumbnail</em>, <em>Mittel</em> und <em>Groß</em>. Wie groß diese Größen tatsächlich
        sind kann unter <em>Einstellungen › Medien</em> festgelegt werden. Beim hochladen eines Bildes werden automatisch auch die definierten Größen angelegt und beim Einfügen
        kann eine der Größen gewählt werden.
    </p>
    <p class="small">
        <em>Ich rede in diesem Abschnitt der Einfachheit halber nur von Beiträgen, gemeint sind aber immer sowohl Beiträge wie auch Seiten und – wenn vorhanden – benutzerdefinierte
            Beitragstypen (Custom Post Types).</em>
    </p>
    <h3 id="medien-mediathek">Mediathek <small class="subhead">Übersicht über alle Medien</small>
    </h3>
    <p>
        …
    </p>
    <h3 id="medien-upload">Medien hochladen <small class="subhead">Neue Bilder für Ihre Homepage</small>
    </h3>
    <p>
        Klicken Sie im Admin-Menü oder in der Mediathek oben neben der Überschrift auf Dateien hinzufügen erscheint ein Bereich zum Hochladen der Medien. Dies kann nur eine Datei
        sein, aber auch mehrere Dateien gleichzeitig. Hier können sie Ihre Medien entweder per Drag-and-Drop hineinziehen <small>(„Ziehen und Loslassen“ aus dem
            Ordnerfenster)</small> , oder Sie wählen den Button <em>Dateien auswählen</em>. Dann erscheint ein Auswahlfenster in dem Sie die gewünschten Medien markieren und dann
        mit dem Button <em>Auswählen</em> das Hochladen starten können.
    </p>
    <p>
        Sobald das Hochladen gestartet wurde sehen Sie weiter unten den Status des Vorgangs. Ist das Hochladen aller Dateien abgeschlossen können Sie mit einem Klick auf <em>Bearbeiten</em>
        die Metadaten der Datei in der Mediathek bearbeiten. (Mehr dazu im <a href="#medien-meta" title="">Abschnitt Metadaten von Medien</a>.)
    </p>
    <p class="small">
        Alternativ ist es auch möglich Medien direkt beim <a href="#medien-einbinden" title="">Einbinden in einen Beitrag</a> hochzuladen. Wählen Sie dazu im Fenster <em>Medien
            hinzufügen</em> oben den Tab <em>Dateien hochladen</em> aus und gehen Sie wie gerade beschrieben vor.
    </p>
    <p>
        Achten Sie beim hochladen der Bilder darauf, dass die Dateien nicht zu groß sind. Die Originaldateien wie sie von der Kamera kommen haben oft Mehrere MegaByte (MB) und eine
        recht hohe Auflösung. Diese hohe Auflösung ist im Web aber meist nicht nötig und zu große Dateien stören, da sie sehr lange zum Laden brauchen.
    </p>
    <p>
        Ich empfehle daher die Bilder <strong>vor</strong> dem Hochladen auf eine Größe von ca. <strong>1800px × 1200px</strong> zu skalieren. Dadurch sinkt auch die Dateigröße auf
        ca 500 – 800 KB. Das müssen nicht die exakten Werte sein. Hat Ihr Bild ein anderes Seitenverhältnis nehmen Sie z.B. eine andere Höhe. Es sind nur Richtwerte die momentan
        ein recht gutes „Preis/Leistungs-Verhältnis“ bieten. <small>(Ladezeit/Auflösung)</small>
    </p>
    <h3 id="medien-einbinden">Medien in Beiträge einbinden <small class="subhead">Bereichern Sie Ihre Texte</small>
    </h3>
    <p>
        Um ein Bild in einen Beitrag einzufügen platzieren Sie den Cursor am Besten <strong>vor</strong> dem Absatz, vor oder neben dem das Bild später erscheinen soll.
        <em class="small">Das hat man mit ein bisschen Übung schnell drauf an welcher Stelle man den Cursor am besten platziert.</em> Anschließend klicken Sie über dem Editor und
        auch über der Werkzeugleiste auf den Button <em>Dateien hinzufügen</em>. Es öffnet sich das Fenster <em>Medien hinzufügen</em>.
    </p>
    <p>
        Sind schon Medien in der Mediathek vorhanden können Sie direkt ein Bild auswählen, sind noch keine Medien in der Mediathek können Sie hier direkt Bilder hochladen.
        Natürlich können Sie hier auch neue Medien hochladen wenn schon Bilder vorhanden sind. Wählen Sie dazu oben den Tab <em>Dateien hochladen</em>.<br /> <small>Das Hochladen
            von Bildern wird im Abschnitt <a href="#medien-upload" title="">Medien hochladen</a> beschrieben.</small>
    </p>
    <p>
        Haben Sie ein Bild durch Klick oder Hochladen ausgewählt sehen Sie auf der rechten Seite die Metadaten des Bildes <small>(Dateianhang-Details)</small> . Diese können Sie
        hier ändern und ergänzen. Vorgenommene Änderungen werden dauerhaft in der Mediathek gespeichert. <small>Mehr dazu im <a href="#medien-meta" title="">Abschnitt Metadaten von
                Meiden</a></small> .
    </p>
    <p>
        Unter den Metadaten finden Sie unter der Überschrift <em>Dateianhang Anzeigeneinstellung</em> einige Optionen zur Darstellung im Beitrag. Hier können Sie die Ausrichtung
        <small>(Links, Rechts, Zentriert, Ohne)</small> und die verwendete Bildgröße festlegen. Außerdem können Sie bestimmen, ob das Bild einen Link haben soll. Dieser kann als
        Ziel direkt das Bild haben (Medien-Datei), eine Anhang-Seite oder eine eigene Adresse. Kein Link ist natürlich auch möglich. All diese Werte können im Nachhinein auch noch
        geändert werden, dazu gleich mehr.
    </p>
    <p>
        Um das Bild jetzt in den Beitrag einzufügen, klicken Sie einfach nur noch auf den Button <em>In den Beitrag einfügen</em> und schon sehen Sie das Bild im Text mit der
        gewählten Größe und Ausrichtung.
    </p>
    <h4>Ein eingefügtes Bild ändern</h4>
    <p>
        Die beim Einfügen vorgenommen Einstellungen können ganz einfach geändert werden.
    </p>
    <p>
        Mit einem Klick auf das Bild erscheint eine kleine Werkzeugleiste direkt über dem Bild. Mit diesen Werkzeugen können Sie die Ausrichtung des Bildes ändern, die
        Eigenschaften bearbeiten oder das Bild aus dem Beitrag entfernen.
    </p>
    <p>
        Beim Bearbeiten sehen Sie oben die Bildunterschrift (Beschriftung) des Bildes und den alternativen Text, der verwendet wird falls das Bild nicht angezeigt werden kann.
        Unterhalb finden Sie die auf dem Hinzufügen-Fenster schon bekannten Einstellung zu Ausrichtung, Größe und Link. Unter dem <em>Erweiterte Funktionen</em> verstecken sich
        noch einige weitere Angaben, die in der Regel aber nicht benötigt werden, daher werde ich hier nicht weiter darauf eingehen.<br /> <small>Der alternative Text ist vor Allem
            auch für sog. Screenreader relevant. Diese werden verwendet um Menschen mit eingeschränkter Sehfähigkeit den Inhalt einer Homepage vorzulesen. </small>
    </p>
    <p>
        Haben Sie versehentlich ein falsches Bild eingefügt können Sie über <em>Ersetzen</em> auf der Rechten Seite unter dem Vorschaubild durch ein anderes Bild der Mediathek
        ersetzen und über <em><a href="#medien-bearbeiten" title="">Original bearbeiten</a></em> können Sie das Bild zuschneiden oder drehen.
    </p>
    <p>
        Um Ihre Änderungen zu bestätigen und zum Beitrag zurückzukehren klicken Sie auf <em>Aktualisieren</em>.
    </p>
    <h3 id="medien-meta">Metadaten von Medien <small class="subhead">Was Ihre Bilder sonst noch ausmacht</small>
    </h3>
    <p>
        Neben dem eigentlichen Bild gibt es noch weitere Informationen die Ihr Bild ausmachen. Die Größe, die Brennweite, die Blendenzahl und noch einiges mehr. Das sind aber die
        Fotografischen Metadaten um die es hier gar nicht gehen soll.
    </p>
    <p>
        Weitere Metadaten sind der Titel des Bildes, die Beschriftung, ein Alternativtext und eine Beschreibung. Diese Metadaten können Sie direkt beim Hochladen eines Bildes
        ändern oder in der Mediathek bzw. beim Einfügen eines Bildes in einen Beitrag.
    </p>
    <ul>
        <li><strong>Titel</strong><br /> Der Titel wird standardmäßig mit dem Dateinamen belegt und wird hauptsächlich für die Benennung in der Mediathek verwendet.
        </li>
        <li><strong>Beschriftung</strong><br /> Die Beschriftung dient als Bildunterschrift in Beiträgen und <a href="#gallery" title="">Bildergalerien</a>
        </li>
        <li><strong>Alternativtext</strong><br /> Der Alternativtext wird verwendet wenn das Bild nicht angezeigt werden kann. Dies ist vor allem für die Barrierefreiheit und die
            Verwendung von Screenreadern wichtig.
        </li>
        <li><strong>Beschreibung</strong><br /> Eine etwas ausführlichere Beschreibung des Bildes die an manchen Stellen im Theme verwendet werden kann.
        </li>
    </ul>
    <p>
        In der Mediathek klicken Sie auf ein Bild. In dem nun erscheinenden Fenster sehen Sie das Bild mit einem Link zum <a href="#medien-bearbeiten" title="">Bearbeiten des
            Bildes</a> sowie den Feldern für die Metadaten.
    </p>
    <p>
        Beim Hinzufügen zu einem Beitrag sehen Sie die Felder für die Metadaten auf der rechten Seiten wenn Sie ein Bild durch Klick ausgewählt haben. Die Daten die Sie hier
        eingeben werden in der Mediathek gespeichert und gelten nicht nur für diesen Beitrag.
    </p>
    <p>
        Außerdem merkt sich WordPress zu welchem Beitrag ein Bild hochgeladen wurde. Diese Information kann – meines Wissens – nicht so einfach geändert werden.
    </p>
    <h3 id="medien-bearbeiten">Bilder bearbeiten <small class="subhead">Zuschneiden, Drehen, Spiegeln</small>
    </h3>
    <p>
        …
    </p>
    <h3 id="medien-beitragsbilder">Beitragsbilder <small class="subhead">Der Lustmacher</small>
    </h3>
    <p>
        Jedem Beitrag und jeder Seite kann ein Beitragsbild angefügt werden, welches dann im Frontend z.B in einem Archiv neben jedem Beitrag angezeigt wird. Die Verwendung der
        Beitragsbilder ist von Theme zu Theme unterschiedlich.
    </p>
    <p>
        Um ein Beitragsbild für einen Beitrag festzulegen finden Sie in der Bearbeitungsansicht auf der rechten Seite eine Box <em>Beitragsbild</em>. Hier können Sie entweder auf
        <em>Beitragsbild hinzufügen</em> klicken um ein Beitragsbild zu verknüpfen oder auf das schon gewählt Bild klicken um es zu ändern.
    </p>
    <p>
        In dem erscheinenden Fester können Sie entweder ein neues Bild hochladen oder ein schon vorhandenes Bild aus der Mediathek wählen. Haben Sie ein Bild ausgewählt fügen Sie
        es mit dem Button <em>Beitragsbild festlegen</em> dem Beitrag hinzu.
    </p>
    <p>
        Ist schon Beitragsbild ausgewählt, können Sie es mit <em>Beitragsbild entfernen</em> wieder löschen. Das Bild verbleibt dann in der Mediathek ist aber nicht mehr als
        Beitragsbild für diesen Beitrag aktiv.
    </p>
    <h3 id="gallery">Galerien <small>Eine einfache Möglichkeit viele Bilder anzuzeigen</small>
    </h3>
    <p>
        Sollen mehrere Bilder auf einmal in einem Beitrag angezeigt werden bringt WordPress standardmäßig eine Galeriefunktion mit.
    </p>
    <p>
        Das Hinzufügen einer Galerie funktioniert ähnlich wie das Hinzufügen von einzelnen Bildern. Platzieren Sie den Cursor an der gewünschten Stelle im Text und Klicken Sie über
        der Werkzeugleiste des Editors auf <em>Dateien hinzufügen</em>.
    </p>
    <p>
        Hier finden Sie jetzt auf der linken Seite ein kleines Menü. Wählen Sie den Punkt <em>Galerie erstellen</em>. Anschließend können Sie Dateien für die Galerie auswählen oder
        neue Medien hochladen. Ist eine Auswahl getroffen <small>und wurden eventuell die <a href="#medien-meta" title="">Metadaten</a> angepasst</small> klicken Sie rechts unten
        auf <em>Erstelle eine neue Galerie</em>.
    </p>
    <p>
        Sie gelangen zum Bearbeitungsmodus der Galerie. Hier sehen Sie alle Bilder der Galerie und können diese auch wieder entfernen. Sollen weiter Bilder in die Galerie eingefügt
        werden wählen Sie Links im Menü den Punkt <em>Zur Galerie hinzufügen</em>. Per Drag and Drop können Sie die Reihenfolge der Bilder ganz einfach ändern. Unter jedem Bild
        sehen Sie Beschriftung des Bildes. Diese wird mit der Beschriftung die in den Metadaten des Bild angegeben ist belegt und kann hier auch direkt geändert werden. Mit einem
        Klick auf eines der Bilder können Sie auf der rechten Seite auch die weiteren Metadaten bearbeiten.<br /> Ebenso in der rechten Spalte finden Sie auch die <em>Galerie-Einstellungen</em>.
        Hier können Sie festlegen wie viele Bilder nebeneinander angezeigt werden sollen, welche Bildgröße verwendet wird und auf welches Ziel die Bilder verlinken sollen.
    </p>
    <p>
        Über den Button <em>Galerie hinzufügen</em> unten rechts platzieren Sie die Galerie im Beitrag.
    </p>
    <p>
        Die Galerie wird im Beitrag als ein Block verstanden. Mit einem Klick auf die Galerie erscheinen direkt bei der Galerie zwei kleine Werkzeuge. Über den Stift gelangen Sie
        wieder zur Bearbeitungsmodus für die Galerie um die Einstellungen anzupassen oder Bilder hinzuzufügen bzw. zu entfernen. Mit dem x können Sie die Galerie wieder entfernen.
    </p>
</section>