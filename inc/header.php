<section id="header">
    <div class="row">
        <img src="/img/header.jpg" width="100%" />
    </div>
    <h1>WordPress für Einsteiger<br /> <small>Ein kleines WordPress-Tutorial für den Schnellstart</small>
    </h1>
    <p class="lead">
        <a href="http://WordPress.org" title="WordPress" target="_blank">WordPress</a> ist eines der populärsten Content Management und Blog Systeme. Es überzeugt, durch seine
        einfache Bedienung und den sehr großen Funktionsumfang.
    </p>
</section>