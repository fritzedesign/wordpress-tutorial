# Ein WordPress-Tutorial für Einsteiger

In diesem Projekt entwickle ich ein einfach zu verstehendes WordPress-Tutorial für Einsteiger. 
Es soll möglichst für jeden verständlich sein, der schonmal einen Computer bedient hat.

Über Feedback und Verbesserungsvorschläge bin ich dankbar, vom simplen Rechtschreibfehler bis hin zu Umformulierungen und neuen Kapiteln.  

[Die Seite kann hier eingesehen werden.](http://wordpress.fritzedesign.de)

## Roadmap

- [ ] Screenshots hinzufügen
- [ ] Hinweise zu Gutenberg einbauen (https://gutenberg-fibel.de)