/*
 * Add fancybox to all links wich references to an image
 * http://fancyapps.com/fancybox/
 */

jQuery( document ).ready( function () {
	// collect all elements for fancybox
	// elements must be links with href to fancybox-content
	var elements = jQuery( 'a[href$="jpg"], a[href$="jpeg"], a[href$="png"], a[href$="gif"], a[href$="svg"]' );

	/* add rel-atribute to all elements to generate a gallery */
	elements.attr( 'rel', 'gallery' );

	// add fancybox-actions to collected elements
	elements.fancybox( {
		padding: 0,
		helpers: {
			title: {type: 'outside'}
		},
		beforeShow: function () {
			/* Disable right click */
			jQuery.fancybox.wrap.bind( "contextmenu", function () {
				return false;
			} );
		},
		beforeLoad: function () {
			/* add wp-caption as displayed title */
			this.title = jQuery( this.element ).next( '.caption' ).html();
		}
	} );
} );