/**
 * Created by LukasFritze on 27.03.17.
 */

var getDirectChildText = function ( element ) {
    return element.contents().filter( function () { return this.nodeType === 3; } )[ 0 ].nodeValue;
};

jQuery( document ).ready( function ( $ ) {

    var nav = $( '#sidenav' );
    console.log( nav );

    var sections = $( 'section' );
    console.log( sections );

    $.each( sections, function () {
        var $section = $( this );

        if ( $section.attr( 'id' ) === 'header' ) {
            return;
        }

        var $headline = $section.children( 'h2' );
        var $subheads = $section.children( 'h3' );

        var listItem = $( '<li></li>' );
        var link = '<a href="#' + $section.attr( 'id' ) + '">' + getDirectChildText( $headline ) + '</a>';
        listItem.append( link );

        if ( $subheads.length > 0 ) {
            var navSub = $( '<ul></ul>', {
                class: 'nav nav-sub'
            } );

            $.each( $subheads, function () {
                var $head = $( this );
                var li = $( '<li></li>' );
                var a = '<a href="#' + $head.attr( 'id' ) + '">' + getDirectChildText( $head ) + '</a>';
                li.append( a );
                navSub.append( li );
            } );

            listItem.append( navSub );
        }

        nav.append( listItem );
    } );

    $( '[data-spy="scroll"]' ).each( function () {
        $( this ).scrollspy( 'refresh' );
    } );

} );